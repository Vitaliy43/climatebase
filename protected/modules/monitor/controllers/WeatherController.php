<?php

class WeatherController extends MonitorController
{

	public function actionSet()
	{
		$message=ClimateMonitor::setClimate($_REQUEST['period']);		
		$this->render('set',array('result'=>$message));

	}
	
	public function actionSunHours()
	{
		$message=ClimateMonitor::setSunHours();
		$this->render('set',array('result'=>$message));

	}
	
	protected function beforeAction($action)
	{
		parent::beforeAction($action);
		return true;
	}
}

?>
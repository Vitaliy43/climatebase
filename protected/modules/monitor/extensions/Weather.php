<?php

class Weather {
	
	
	public $table='';
	public $sumat;
	public $q_temp;
	public $num_years;
	public $get=false;
	public $field_precip;
	public $period='';
	protected $station;
	public $is_all=false;
	public $is_tutiempo=false;
	public $begin_year=1970;
	protected $num_years_for_precip;
	protected $period_for_precip;
		
	function __construct($table,$station) {
		
		$this->table=$table;
		$this->station=$station;
		$this->init();
			
	}
	
	function init($is_all=false){
		
		
		$sql="SELECT COUNT(id) AS amt FROM $this->table WHERE station=$this->station $this->period";
		$buffer=Yii::app()->db->createCommand($sql)->queryRow();
		$this->num_years=$buffer['amt']/365.25;

		if($is_all):
		if($this->is_all and $this->is_tutiempo){
			$sql="SELECT COUNT(id) AS amt FROM $this->table WHERE station=$this->station AND year BETWEEN $this->begin_year AND 2000";
			$buffer_precip=Yii::app()->db->createCommand($sql)->queryRow();
			$this->num_years_for_precip=$buffer_precip['amt']/365.25;
		}
		elseif($this->is_all){
			$sql="SELECT COUNT(id) AS amt FROM $this->table WHERE station=$this->station AND year>=$this->begin_year";
			$buffer_precip=Yii::app()->db->createCommand($sql)->queryRow();
			$this->num_years_for_precip=$buffer_precip['amt']/365.25;
		}
		endif;

		return $buffer['amt'];
	}
	
	
	function get_t1($station,$year=null,$month=null){
		
		if($month)
			$month="AND month=$month";
		else
			$month='';
			
		if($year)
			$year="AND year=$year";
		else
			$year='';
		
		$sql="SELECT AVG(mintemp) AS avg_min FROM $this->table WHERE station=$station $month $year $this->period";
		$res=Yii::app()->db->createCommand($sql)->queryRow();	
		$result=round($res['avg_min'],1);
		if($this->get)
			$result=sprintf("%01.1f",$result);
		return $result;
	}
	
	function get_midtemp($station,$year=null,$month=null){
		
		if($month)
			$month="AND month=$month";
		else
			$month='';
			
		if($year)
			$year="AND year=$year";
		else
			$year='';
		
		$sql="SELECT AVG(midtemp) AS avg_mid FROM $this->table WHERE station=$station $month $year $this->period";
		$res=Yii::app()->db->createCommand($sql)->queryRow();	
		$result=round($res['avg_mid'],1);
		if($this->get)
			$result=sprintf("%01.1f",$result);
		return $result;
	}
	
	function get_t2($station,$year=null,$month=null){
		
		if($month)
			$month="AND month=$month";
		else
			$month='';
			
		if($year)
			$year="AND year=$year";
		else
			$year='';
		
		$sql="SELECT AVG(maxtemp) AS avg_max FROM $this->table WHERE station=$station $month $year $this->period";
		$res=Yii::app()->db->createCommand($sql)->queryRow();	
		$result=round($res['avg_max'],1);
		if($this->get)
			$result=sprintf("%01.1f",$result);
		return $result;
	}
	
	function get_abs_min($station,$year=null,$month=null){
		
		if($month)
			$month="AND month=$month";
		else
			$month='';
			
		if($year)
			$year="AND year=$year";
		else
			$year='';
		
		$sql="SELECT MIN(mintemp) AS abs_min FROM $this->table WHERE station=$station $month $year $this->period";
		$res=Yii::app()->db->createCommand($sql)->queryRow();	
		$result=round($res['abs_min'],1);
		if($this->get)
			$result=sprintf("%01.1f",$result);
		return $result;
	}
	
	function get_abs_max($station,$year=null,$month=null){
		
		if($month)
			$month="AND month=$month";
		else
			$month='';
			
		if($year)
			$year="AND year=$year";
		else
			$year='';
		
		$sql="SELECT MAX(maxtemp) AS abs_max FROM $this->table WHERE station=$station $month $year $this->period";
		$res=Yii::app()->db->createCommand($sql)->queryRow();	
		$result=round($res['abs_max'],1);
		if($this->get)
			$result=sprintf("%01.1f",$result);
		return $result;
	}
	
	function get_precipitation($station,$year=null,$month=null){
				
		if($month)
			$month="AND month=$month";
		else
			$month='';
			
		if($year)
			$year="AND year=$year";
		else
			$year='';
			
	
			
		if($this->is_all and !$this->is_tutiempo){
			$sql="SELECT SUM($this->field_precip) AS sum_precip FROM $this->table WHERE station=$station $month $year AND year>=$this->begin_year";
		}
			
		elseif($this->is_all and $this->is_tutiempo){
			$sql="SELECT SUM($this->field_precip) AS sum_precip FROM $this->table WHERE station=$station $month $year AND year BETWEEN $this->begin_year AND 2000";
		}
		
		else{
			$sql="SELECT SUM($this->field_precip) AS sum_precip FROM $this->table WHERE station=$station $month $year $this->period";
		}
		
//		echo $sql.';';
			
		$res=Yii::app()->db->createCommand($sql)->queryRow();	
		if($year==null){
			if($this->is_all)
				$result=$res['sum_precip']/$this->num_years_for_precip;
			else
				$result=$res['sum_precip']/$this->num_years;
				
		}
		else{
			$result=$res['sum_precip'];

		}
			$result=round($result,1);

		if($this->get)
			$result=sprintf("%01.1f",$result);		
		return $result;
	}	
	
	
	function get_precip_days($station,$year=null,$month=null){
				
		if($month)
			$month="AND month=$month";
		else
			$month='';
			
		if($year)
			$year="AND year=$year";
		else
			$year='';

			if($this->is_all and !$this->is_tutiempo)
				$sql="SELECT COUNT(*) AS count_precip FROM $this->table WHERE station=$station $month $year AND $this->field_precip>0 AND year>=$this->begin_year";
			elseif($this->is_all and $this->is_tutiempo)
				$sql="SELECT COUNT(*) AS count_precip FROM $this->table WHERE station=$station $month $year AND $this->field_precip>0 AND year BETWEEN $this->begin_year AND 2000";
			
			else
				$sql="SELECT COUNT(*) AS count_precip FROM $this->table WHERE station=$station $month $year AND $this->field_precip>0 $this->period";
				
		$res=Yii::app()->db->createCommand($sql)->queryRow();
		
		if($year==null){
			if($this->is_all)
				$result=$res['count_precip']/$this->num_years_for_precip;
			else
				$result=$res['count_precip']/$this->num_years;

		}
		else{
			$result=$res['count_precip'];

		}
			$result=round($result,1);

		if($this->get)
			$result=sprintf("%01.1f",$result);		
		return $result;
			
		
	}
	
	function get_sum_at($station,$year=null,$month=null){
		
		
		if($month)
			$month="AND month=$month";
		else
			$month='';
		if($year)
			$year="AND year=$year";
		else
			$year='';
		
		$sql="SELECT SUM(midtemp) AS count_precip FROM $this->table WHERE station=$station $month $year AND midtemp>=10 $this->period";
		$res=Yii::app()->db->createCommand($sql)->queryRow();	
		if($year==null)
			$result=$res['count_precip']/$this->num_years;
		else
			$result=$res['count_precip'];
			$result=round($result);
		
		if($this->get)
			$result=sprintf("%01.1f",$result);		
		return $result;
		
	
	}
	
	function get_clear_days($station,$year=null,$month=null){
			
	
		if($month)
			$month="AND month=$month";
		else
			$month='';
		if($year)
			$year="AND year=$year";
		else
			$year='';
		
		$sql="SELECT COUNT(*) AS sum_precip FROM $this->table WHERE station=$station $month $year AND cloudness<2.5";
		$res=Yii::app()->db->createCommand($sql)->queryRow();	
		if($year==null)
			$result=$res['sum_precip']/$this->num_years;
		else
			$result=$res['sum_precip'];
			$result=round($result,1);
		
		if($this->get)
			$result=sprintf("%01.1f",$result);		
		return $result;
		
	}
	
	function get_average_cloudness($station,$year=null,$month=null){
				
		if($month)
			$month="AND month=$month";
		else
			$month='';
			
		if($year)
			$year="AND year=$year";
		else
			$year='';
			
		$sql="SELECT AVG(cloudness) AS sum_precip FROM $this->table WHERE station=$station $month $year";
		$res=Yii::app()->db->createCommand($sql)->queryRow();	
		
		$result=round($res['sum_precip'],1);
		
		if($this->get)
			$result=sprintf("%01.1f",$result);		
		return $result;
		
	}
	
	function get_cloudy_days($station,$year=null,$month=null){
				
		if($month)
			$month="AND month=$month";
		else
			$month='';
			
		if($year)
			$year="AND year=$year";
		else
			$year='';
		
		$sql="SELECT COUNT(*) AS sum_precip FROM $this->table WHERE station=$station $month $year AND cloudness>=2.5 AND cloudness<8";
		$res=Yii::app()->db->createCommand($sql)->queryRow();	
		if($year==null)
			$result=$res['sum_precip']/$this->num_years;
		else
			$result=$res['sum_precip'];
			$result=round($result,1);
		
		if($this->get)
			$result=sprintf("%01.1f",$result);		
		return $result;
		
	}
	
	function get_overcast_days($station,$year=null,$month=null){
				
		if($month)
			$month="AND month=$month";
		else
			$month='';
			
		if($year)
			$year="AND year=$year";
		else
			$year='';
		
		$sql="SELECT COUNT(*) AS sum_precip FROM $this->table WHERE station=$station $month $year AND cloudness>=8";
		$res=Yii::app()->db->createCommand($sql)->queryRow();	
		if($year==null)
			$result=$res['sum_precip']/$this->num_years;
		else
			$result=$res['sum_precip'];
			$result=round($result,1);
		if($this->get)
			$result=sprintf("%01.1f",$result);		
		return $result;
	}
	
	
}

?>